package test

import (
	"testing"

	"gitlab.com/gunmurat/fizzbuzzapi/service"
	"gotest.tools/assert"
)

var serv = service.NewFizzBuzzService()

func TestFizzBuzzFizz(t *testing.T) {

	assert.Equal(t, "Fizz", serv.GetFizzBuzz(3))

}

func TestFizzBuzzBuzz(t *testing.T) {

	assert.Equal(t, "Buzz", serv.GetFizzBuzz(5))

}

func TestFizzBuzzFizzBuzz(t *testing.T) {

	assert.Equal(t, "FizzBuzz", serv.GetFizzBuzz(15))

}
