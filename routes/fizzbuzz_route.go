package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/fizzbuzzapi/controllers"
	"gitlab.com/gunmurat/fizzbuzzapi/service"
)

var controller = controllers.NewFizzBuzzController(service.NewFizzBuzzService())

func FizzBuzzRoutes(app *fiber.App) {
	app.Get("/fizzbuzz/:number", controller.GetFizzBuzz)
}
