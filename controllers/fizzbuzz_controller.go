package controllers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/fizzbuzzapi/service"
)

type FizzBuzzControllerInt interface {
	GetFizzBuzz(ctx *fiber.Ctx) error
}

type FizzBuzzController struct {
	fizzbuzzService service.FizzBuzzServiceInterface
}

func NewFizzBuzzController(service service.FizzBuzzServiceInterface) FizzBuzzControllerInt {
	return &FizzBuzzController{
		fizzbuzzService: service,
	}
}

func (c *FizzBuzzController) GetFizzBuzz(ctx *fiber.Ctx) error {
	number := ctx.Params("number")

	intNumber, err := strconv.Atoi(number)
	if err != nil {
		return ctx.Status(400).SendString("Invalid number")
	}

	result := c.fizzbuzzService.GetFizzBuzz(intNumber)

	return ctx.Status(200).JSON(fiber.Map{
		"fizzBuzz": result,
	})

}
