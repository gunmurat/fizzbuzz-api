package service

type FizzBuzzServiceInterface interface {
	GetFizzBuzz(number int) string
}

type fizzBuzzService struct {
}

func NewFizzBuzzService() FizzBuzzServiceInterface {
	return &fizzBuzzService{}
}

func (c *fizzBuzzService) GetFizzBuzz(number int) string {
	if number%15 == 0 {
		return "FizzBuzz"
	} else if number%3 == 0 {
		return "Fizz"
	} else if number%5 == 0 {
		return "Buzz"
	}
	return string(number)
}
